/**
 * Created by k_zenchyk on 12/15/15.
 */
function floorRange(val){
    /* change number of floor by range input*/
  this.value = val;
  var numFl = document.getElementById('innum').value;
  if (val != numFl) {
    document.getElementById('innum').value = val;
    drawHouse(val);
  }
}

function floorNum(val){
    /* change number of floor by num input*/
  this.value = val;
  var numFl = document.getElementById('inrange').value;
  if (val != numFl && (val>=1 && val <=5)) {
      document.getElementById('inrange').value = val;
      drawHouse(val);
  }
}

function roof(){
    /* draw roof */
  var myRoof = document.createElement('div');
  //myRoof.innerHTML = 'ROOF';
  myRoof.setAttribute('class', 'triangle-up ');

  return myRoof;
}

function drawWindow(){
    /* draw windows on floor */
  var myWinds = document.createElement('div');
  var myWindow1= document.createElement('span');
  var myWindow2= document.createElement('span');
  myWindow1.setAttribute('class', 'myWindow');
  myWindow2.setAttribute('class', 'myWindow');
  myWinds.appendChild(myWindow1);
  myWinds.appendChild(myWindow2);

  return myWinds;
}

function drawDoor() {
    /* draw door */
  var myDoor = document.createElement('div');
  var myDoor1= document.createElement('span');
  myDoor1.setAttribute('class', 'myDoor');
  myDoor.appendChild(myDoor1);

  return myDoor;
}

function floor(parentId){
    /* create floor div as parent for drawWindow() */
  var myFloor = document.createElement('div');
  parentId.appendChild(myFloor);
  //myFloor.innerHTML = 'FLOOR ' + floorNumber;
  myFloor.appendChild(drawWindow());

}

function drawBody(parentId){
    /* draw house body border */
  var myBody = document.createElement('div');
  myBody.setAttribute('class', 'myBody');
  parentId.appendChild(myBody);

  return myBody;
}

function drawHouse(countFloor){
    /* draw house with countFloor numbers of floors*/
  console.log("countFloor ",countFloor);
  var myNode = document.getElementById("house");
  while (myNode.firstChild) {
    myNode.removeChild(myNode.firstChild);
  }

  myNode.appendChild(roof());
  var BodyId = drawBody(myNode);
    for(var i = countFloor; i>=1; i--){
      floor(BodyId);
      console.log(i);
    }
  BodyId.appendChild(drawDoor());
}

drawHouse(1);