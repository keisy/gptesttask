# README #

It is test task with two applications.

## appRandomizer ##
App creates a noun consisted with random adjective and noun.
Second part use ajax request to obtain dictionary.

## appHome ##
Draw house with required number of floors.


# QUICK START #
### appRandomizer ###
1. Clone this repo
2. Open appRandomizer/index1.html in browser (preferably use Firefox or Chrome).
3. Open JS console at Devepopers tools and run randomizer() function
4*. Open appRandomizer/index2.html and run randomizer() to get noun with ajax query to dictionary.

### appHome ###
1. Clone this repo
2. Open appHome/house.html in browser (preferably use Firefox or Chrome).
3. Enjoy

