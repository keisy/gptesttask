/**
 * Created by k_zenchyk on 12/13/15.
 */

var __uniqQ = [];
var dict = {};

function loadDictionary () {

    fetch('https://fathomless-everglades-3680.herokuapp.com/api/dictionary')
      .then(
        function(response) {
          if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' +
              response.status);
            return;
          }
          // Examine the text in the response
          response.json().then(function(data) {
              //console.log(data);
              dict = data;
            });
        }
      )
      .catch(function(err) {
        console.log('Fetch Error :-S', err);
      });
}

loadDictionary();

function getRandomElement(lst){
    var wordNumber = Math.floor(Math.random() * (lst.length));
    return lst[wordNumber];
}

function capitalizeWord(word){
    return word[0].toUpperCase() + word.slice(1);
}

function randomizer() {
    if (dict.length == 0) {
        loadDictionary();
        return "Loading data. Please, try again later"
    }
    return getRandomPair().map(capitalizeWord).join('');
}

function getRandomPair() {

    var noun = getRandomElement(dict.nouns);
    var adj = getRandomElement(dict.adjectives);
    var resPair = [noun, adj];
    var pair = noun + adj;

    if (__uniqQ.indexOf(pair) >= 0) {
        // recursive call

        resPair = getRandomPair();
    } else {
        var ind = __uniqQ.push(pair);
        if (ind > 10) {
            __uniqQ.shift();
        }
    }

    return resPair
}


// Export to global scope
window.randomizer = randomizer;