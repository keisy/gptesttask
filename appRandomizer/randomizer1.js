/**
 * Created by k_zenchyk on 12/13/15.
 */
var nouns =  ["time","person","year","way","day"];
var adjectives = ["different","used","important","every","large"];


var __uniqQ = [];

function getRandomElement(lst){
    var wordNumber = Math.floor(Math.random() * (lst.length));
    return lst[wordNumber];
}

function capitalizeWord(word){
    return word[0].toUpperCase() + word.slice(1);
}

function randomizer() {

    return getRandomPair().map(capitalizeWord).join('');

}

function getRandomPair() {
    var noun = getRandomElement(nouns);
    var adj = getRandomElement(adjectives);
    var resPair = [adj, noun];
    var pair = adj + noun;

    if (__uniqQ.indexOf(pair) >= 0){
        /** recursive call
         * number of pair should be no less than 11 or recursion will be infinite
         */
        resPair = getRandomPair();
    } else {
        var ind = __uniqQ.push(pair);
        if (ind > 10) {
            __uniqQ.shift();
        }
    }

    return resPair
}


// Export to global scope
window.randomizer = randomizer;